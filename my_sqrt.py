"""
Module to approximate square root function
"""
def my_sqrt(x,debug=False):
    
    from numpy import nan
    if x==0.:
        return 0.
    elif x<0:
        print 'ERROR: x must be non-negative'
        return nan
#    assert x>0. and type(x) is not string, 'Unrecognized input'
    assert x>0., 'Unrecognized input'
    s=1.
    kmax=100
    tol=1.0e-14
    for k in range(kmax):
        """
        more info
        """
        if debug:
            print 'BEFORE iteration %s, s = %20.15e' % (k,s)
        s0=s
        s=0.5*(s + x/s)
        delta_s=abs(s-s0)
        if debug:
            print 'delta s = %s' % delta_s
        if(delta_s/x < tol):
            break
    if debug:
        print 'AFTER iteration %s, s = %20.15f' % (k+1,s)    
    return s

def test():
    from numpy import sqrt
    xvalues=[0.,2.,100.,1000.,1.e-4]
    for x in xvalues:
        print 'testing with x = %20.15e' % x
        my_s=my_sqrt(x)
        numpy_s=sqrt(x)
        print 'sqrt(%s) from numpy = %20.15e' % (x,numpy_s)
        print 'sqrt(%s) from code  = %20.15e' % (x,my_s)
        assert abs(my_s - numpy_s) < 1.e-14, \
        'Disagree for x = %20.15e' % x
        print ' ---------'